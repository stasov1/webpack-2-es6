/* eslint-disable */
'use strict';

import webpack from 'webpack';
import path from 'path';

import HtmlWebpackPlugin from 'html-webpack-plugin';
import CombineLoaders from 'webpack-combine-loaders';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev';
const NODE_PORT = process.env.NODE_PORT ? process.env.NODE_PORT : 9092;

let styleLoaders;

if (NODE_ENV == 'dev') {
  styleLoaders = {
    loader: CombineLoaders([
      { loader: 'style-loader' },
      {
        loader: 'css-loader',
        options: {
          module: true,
          importLoaders: 1,
          localIdentName: '[name]__[local]__[hash:base64:5]'
        }
      },
      { loader: 'postcss-loader' }
    ])
  };
} else {
  styleLoaders = {
    loader: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        {
          loader: 'css-loader',
          options: {
            module: true,
            minimize: true,
            importLoaders: 1,
            localIdentName: '[hash:base64:5]'
          }
        },
        'postcss-loader'
      ]
    })
  };
}

const config = {
  context: path.join(__dirname, '/src/client/'),

  entry: {
    index: ['babel-polyfill', './index']
  },

  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: '/'
  },

  resolve: {
    modules: [path.resolve('node_modules')],
    extensions: ['.web.js', '.js', '.jsx', '.json']
  },

  devtool: NODE_ENV == 'dev' ? 'eval' : false,

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        enforce: 'pre',
        loader: 'eslint-loader',
        exclude: /node_modules/,
        include: __dirname
      },
      {
        test: /\.jsx?$/,
        use: [
          { loader: 'react-hot-loader/webpack' },
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['es2015', { modules: false }],
                'stage-0',
                'stage-2',
                'react'
              ]
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        ...styleLoaders
      },
      {
        test: /\.woff2?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]?[hash:base64:5]'
          }
        }]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1000,
              name: '[path][name].[ext]?[hash:base64:5]'
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              progressive: true,
              optimizationLevel: 7,
              interlaced: false,
              pngquant: { quality: "65-90", speen: 4 }
            }
          }
        ]
      }
    ]
  },

  devServer: {
    contentBase: path.join(__dirname, 'build'),
    hot: true,
    inline: true,
    host: 'localhost',
    port: NODE_PORT,
    historyApiFallback: true,
    stats: {
      colors: true
    },
    // proxy: {
    //   '/api/*': {
    //     target: 'http://yourSite',
    //     changeOrigin: true,
    //     secure: false
    //   }
    // }
  },

  plugins: [
    new ExtractTextPlugin('css/style.css'),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(NODE_ENV),
        'NODE_PORT': JSON.stringify(NODE_PORT)
      }
    }),
    new HtmlWebpackPlugin({
      template: 'index.ejs',
      inject: 'body',
      hash: true
    })
  ]
};

if (NODE_ENV == 'prod') {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false
    })
  )
}

if (NODE_ENV == 'dev') {
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  )
}

export default config;

/* eslint-enable */
