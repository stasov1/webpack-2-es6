import Express from 'express';
import mongoose from 'mongoose';
import crypto from 'crypto';

import '../models/User';

const router = Express.Router();
const User = mongoose.model('User');

// Create new user
router.post('/create', (req, res) => {
  if (!req.body.login || !req.body.password) {
    return res.status(400).send({
      message: 'Не верный запрос'
    });
  }

  const login = req.body.login;
  const password = generateHash(req.body.password);

  const userPromise = User.findOne({ login: login }).exec();

  userPromise
    .then(user => {
      if (!user) {
        const user = new User({
          login: login,
          password: password
        });

        return user.save().then(user => res.send(user));
      } else {
        return res.send({ message: 'Пользователь с таким логином уже существует' });
      }
    })
    .catch(error => {
      return res.send({
        status: 500,
        message: 'Ошибка',
        error: error
      });
    });
});

// Delete user
router.delete('/delete', (req, res) => {
  if (!req.body.login || !req.body.password) {
    return res.status(400).send({
      message: 'Не верный запрос'
    });
  }

  const userPromise = User.findOne({ login: req.body.login }).exec();

  userPromise
    .then(user => {
      return user.remove();
    })
    .then(user => {
      return res.send(user);
    })
    .catch(error => {
      return res.status(500).send({
        message: 'Ошибка',
        error: error
      });
    });
});

// User auth
router.post('/auth', (req, res) => {
  if (!req.body.login || !req.body.password) {
    return res.status(400).send({
      message: 'Не верный запрос'
    });
  }

  const login = req.body.login;
  const password = generateHash(req.body.password);

  const userPromise = User.findOne({ login: login }).exec();

  userPromise
    .then(user => {
      if (!user) {
        return res.status(404).send({
          message: 'Не верный логин или пароль'
        });
      }
      if (user.password == password) {
        req.session.name = login;

        return res.redirect('/admin');
      } else {
        return res.send({
          status: 300,
          message: 'Отказано в доступе'
        });
      }
    })
    .catch(error => {
      return res.send({
        status: 300,
        message: 'Ошибка',
        error: error
      });
    });
});

// Logout from admin panel
router.post('/logout', (req, res) => {
  req.session = null;
  
  return res.redirect('/auth');
});

function generateHash(pwd) {
  return crypto.createHash('sha256').update(pwd).digest('base64');
}

export default router;
