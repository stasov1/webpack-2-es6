import Express from 'express';
import mongoose from 'mongoose';

import '../models/Todo';

// import * as Todos from '../utils/todos';

const router = Express.Router();
const Todo = mongoose.model('Todo');

router.get('/', (req, res) => {
  return Todo.find().then(todos => {
    console.log(req.session);
    res.send(todos);
  });
});

router.get('/:id', (req, res) => {
  return Todo.findById(req.params.id, (error, todo) => {
    if (error) {
      res.status(404).send({
        message: 'Entry not found',
        findId: req.params.id
      });
    } else {
      return res.send(todo);
    }
  });
});

router.post('/', (req, res) => {
  const todo = new Todo({
    title: req.body.title,
    text: req.body.text
  });

  return todo.save().then(todo => res.send(todo));
});

router.put('/:id', (req, res) => {
  Todo.findById(req.params.id, (error, todo) => {
    if (error) {
      res.status(500).send(error);
    } else {
      todo.title = req.body.title || todo.title;
      todo.text = req.body.text || todo.text;
      todo.completed = req.body.completed || todo.completed;
      todo.date = Date.now();

      todo.save();

      return res.send(todo);
    }
  });
});

router.delete('/:id', (req, res) => {
  return Todo.findById(req.params.id).remove()
    .then(todo => res.send(todo));
});

export default router;
