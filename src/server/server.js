import Express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cookieSession from 'cookie-session';
import path from 'path';

import setConnect from './utils/setConnect';

import todos from './routes/todos';
import auth from './routes/auth';

const app = Express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cookieParser('notSecret'));
app.use(cookieSession({ secret: 'notsecret' }));

app.use(Express.static(path.join(__dirname, '/server/public')));

setConnect();

app.use('/api/todos', todos);
app.use('/api/user', auth);

app.get('/auth', (req, res) => {
  res.render('auth');
});

app.get('/admin', (req, res) => {
  if (req.session.name) {
    res.render('admin');
  } else {
    res.redirect('/auth');
  }
});

app.listen(port, () => {
  console.log(`Server start on ${port} port`);
});
