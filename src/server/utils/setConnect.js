import { NAME, PASSWORD } from '../config/database';

import mongoose from 'mongoose';

export default function setConnect() {
  return mongoose.connect(`mongodb://${NAME}:${PASSWORD}@ds139959.mlab.com:39959/notes`);
}
