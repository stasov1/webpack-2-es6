import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const TodoSchema = new Schema({
  title: String,
  text: String,
  completed: {
    type: Boolean,
    default: false
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

const Todo = mongoose.model('Todo', TodoSchema);

export default Todo;
