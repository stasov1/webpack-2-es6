import React from 'react';
import { render } from 'react-dom';

import './base/typography.css';
import './base/index.css';

import App from './containers/App';

render(
  <App />,
  document.getElementById('app')
);
